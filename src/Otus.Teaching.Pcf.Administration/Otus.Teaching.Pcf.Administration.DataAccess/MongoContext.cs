﻿using MongoDB.Driver;

namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    public class MongoContext
    {
        private readonly IMongoDatabase _db;

        public MongoContext(IMongoClient client, string dbName)
        {
            _db = client.GetDatabase(dbName);
        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return _db.GetCollection<T>(name.ToLower());
        }
        //public IMongoCollection<Role> Roles => _db.GetCollection<Role>("role");
        //public IMongoCollection<Employee> Employees => _db.GetCollection<Employee>("employee");
    }
}