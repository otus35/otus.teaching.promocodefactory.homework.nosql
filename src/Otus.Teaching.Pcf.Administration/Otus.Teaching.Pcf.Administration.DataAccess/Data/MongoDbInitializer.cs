﻿
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _db;
        private readonly IMongoCollection<Employee> _collectionEmployee;
        private readonly IMongoCollection<Role> _collectionRole;
        private readonly string collectionNameEmployee = typeof(Employee).Name.ToLower();
        private readonly string collectionNameRole = typeof(Role).Name.ToLower();

        public MongoDbInitializer(IMongoClient client, string dbName)
        {
            _db = client.GetDatabase(dbName);
            _collectionEmployee = _db.GetCollection<Employee>(collectionNameEmployee);
            _collectionRole = _db.GetCollection<Role>(collectionNameEmployee);
        }

        public void InitializeDb()
        {
            _db.DropCollection(collectionNameEmployee);
            _db.DropCollection(collectionNameRole);
            _collectionEmployee.InsertMany(FakeDataFactory.Employees);
            _collectionRole.InsertMany(FakeDataFactory.Roles);
        }
    }
}