﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class CustomerMongoRepository : MongoRepository<Customer>, IRepository<Customer>
       
    {
        
        private readonly MongoContext _db;
        private readonly IMongoCollection<Customer> _customerCollection;
        private readonly IMongoCollection<Preference> _preferenceCollection;
        private readonly IMongoCollection<PromoCode> _promoCodeCollection;
        public CustomerMongoRepository(MongoContext db) : base(db)
        {
            _db = db;
            _customerCollection = _db.GetCollection<Customer>(typeof(Customer).Name.ToLower());
            _preferenceCollection = _db.GetCollection<Preference>(typeof(Preference).Name.ToLower());
            _promoCodeCollection = _db.GetCollection<PromoCode>(typeof(PromoCode).Name.ToLower());
        }

        public override async Task<Customer> GetByIdAsync(Guid id)
        {

            try
            {
                var entity = await _customerCollection.Aggregate()
                    .Lookup<Customer, Preference, Customer>(
                                               _preferenceCollection,
                                               localField => localField.PreferenceIds,
                                               foreignField => foreignField.Id,
                                               output => output.Preferences)
                    .Lookup<Customer, PromoCode, Customer>(
                                               _promoCodeCollection,
                                               localField => localField.PromoCodeIds,
                                               foreignField => foreignField.Id,
                                               output => output.PromoCodes).ToListAsync();

                return entity.FirstOrDefault();
            }
            catch(Exception ee)
            {
                throw new Exception("", ee);
            }
            return new Customer();
        }
    }
}