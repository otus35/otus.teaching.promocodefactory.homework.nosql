﻿using MongoDB.Driver;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess
{
    public class MongoContext
    {
        private readonly IMongoDatabase _db;

        public MongoContext(IMongoClient client, string dbName)
        {
            _db = client.GetDatabase(dbName);
        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return _db.GetCollection<T>(name.ToLower());
        }
    }
}