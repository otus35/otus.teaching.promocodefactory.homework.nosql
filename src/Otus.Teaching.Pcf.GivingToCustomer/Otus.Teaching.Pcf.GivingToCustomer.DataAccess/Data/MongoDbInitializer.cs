﻿
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _db;
        private readonly IMongoCollection<Preference> _collectionPreference;
        private readonly IMongoCollection<Customer> _collectionCustomer;        
        private readonly string collectionNamePreference = typeof(Preference).Name.ToLower();
        private readonly string collectionNameCustomer = typeof(Customer).Name.ToLower();
        private readonly string collectionNamePromoCode = typeof(PromoCode).Name.ToLower();

        public MongoDbInitializer(IMongoClient client, string dbName)
        {
            _db = client.GetDatabase(dbName);
            _collectionPreference = _db.GetCollection<Preference>(collectionNamePreference);
            _collectionCustomer = _db.GetCollection<Customer>(collectionNameCustomer);
        }

        public void InitializeDb()
        {
            _db.DropCollection(collectionNamePreference);
            _db.DropCollection(collectionNameCustomer);
            _db.DropCollection(collectionNamePromoCode);

            _collectionPreference.InsertMany(FakeDataFactory.Preferences);
            _collectionCustomer.InsertMany(FakeDataFactory.Customers);
        }
    }
}