﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapFromModel(GivePromoCodeRequest request, Guid preferenceId, IEnumerable<Guid> customerIds)
        {
            var promocode = new PromoCode()
            {
                Id = Guid.NewGuid(),

                PartnerId = request.PartnerId,
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,

                BeginDate = DateTime.Parse(request.BeginDate),
                EndDate = DateTime.Parse(request.EndDate),

                PreferenceId = preferenceId,
                CustomerIds = customerIds.ToList()
            };

            return promocode;
        }
    }
}
