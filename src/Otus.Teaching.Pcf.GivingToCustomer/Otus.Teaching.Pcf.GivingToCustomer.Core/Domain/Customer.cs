﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class Customer
        :BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }
                
        public ICollection<Guid> PreferenceIds { get; set; }
        public ICollection<Guid> PromoCodeIds { get; set; }
        [BsonIgnoreIfNull]
        public ICollection<Preference> Preferences { get; set; }
        [BsonIgnoreIfNull]
        public ICollection<PromoCode> PromoCodes { get; set; }
        
    }
}