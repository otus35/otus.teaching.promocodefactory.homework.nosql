﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class PromoCode
        : BaseEntity
    {
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public Guid PartnerId { get; set; }

        public Guid PreferenceId { get; set; }

        [BsonIgnore]
        public Preference Preference { get; set; }

        public ICollection<Guid> CustomerIds { get; set; }

        [BsonIgnore]
        public ICollection<Customer> Customers { get; set; }
    }
}